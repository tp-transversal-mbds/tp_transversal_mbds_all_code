// personController.js
// Import person model
Person = require('./personModel');
// Handle index actions
exports.index = function (req, res) {
    Person.get(function (err, persons) {
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        }
        res.json({
            status: "success",
            message: "persons retrieved successfully",
            data: persons
        });
    });
};
// Handle create person actions
exports.new = function (req, res) {
    var person = new Person();
    person.nom = req.body.nom ? req.body.nom : person.nom;
    person.prenom = req.body.prenom;
    person.dtn = req.body.dtn;
    person.ln = req.body.ln;
    person.pere = req.body.pere;
    person.mere = req.body.mere;
    person.tel = req.body.tel;
    person.cin.numero = req.body.cin.numero;
    person.cin.uid_nfc = req.body.cin.uid_nfc;
    person.cin.photo = req.body.cin.photo;
    person.cin.date_delivrance = req.body.cin.date_delivrance;
    person.cin.lieu_delivrance = req.body.cin.lieu_delivrance;
    person.cin.profession = req.body.cin.profession;
    person.cin.domicile = req.body.cin.domicile;
    person.cin.fokontany.id = req.body.cin.fokontany.id;
    person.cin.fokontany.val = req.body.cin.fokontany.val;
    person.cin.fokontany.desc = req.body.cin.fokontany.desc;
    person.cin.est_valide = req.body.cin.est_valide;
    person.bv.id = req.body.bv.id;
    person.bv.val = req.body.bv.val;
    person.bv.desc = req.body.bv.desc;
// save the person and check for errors
    person.save(function (err) {
        if (err)
            res.json(err);
res.json({
            message: 'New person created!',
            data: person
        });
    });
};
// Handle view person info
exports.view = function (req, res) {
    Person.findById(req.params.person_id, function (err, person) {
        if (err)
            res.send(err);
        res.json({
            message: 'person details loading..',
            data: person
        });
    });
};
// Handle liste bv person info
exports.liste_person_bv = function (req, res) {
    Person.find({"bv.id": req.params.bv_id,}, function (err, person) {
        if (err)
            res.send(err);
        res.json({
            message: 'person details loading..',
            data: person
        });
    });
};
// Handle update person info
exports.update = function (req, res) {
Person.findById(req.params.person_id, function (err, person) {
        if (err)
            res.send(err);
person.nom = req.body.nom ? req.body.nom : person.nom;
person.prenom = req.body.prenom;
person.dtn = req.body.dtn;
person.ln = req.body.ln;
person.pere = req.body.pere;
person.mere = req.body.mere;
person.tel = req.body.tel;
person.cin.numero = req.body.cin.numero;
person.cin.uid_nfc = req.body.cin.uid_nfc;
person.cin.photo = req.body.cin.photo;
person.cin.date_delivrance = req.body.cin.date_delivrance;
person.cin.lieu_delivrance = req.body.cin.lieu_delivrance;
person.cin.profession = req.body.cin.profession;
person.cin.domicile = req.body.cin.domicile;
person.cin.fokontany.id = req.body.cin.fokontany.id;
person.cin.fokontany.val = req.body.cin.fokontany.val;
person.cin.fokontany.desc = req.body.cin.fokontany.desc;
person.cin.est_valide = req.body.cin.est_valide;
person.bv.id = req.body.bv.id;
person.bv.val = req.body.bv.val;
person.bv.desc = req.body.bv.desc;
// save the person and check for errors
        person.save(function (err) {
            if (err)
                res.json(err);
            res.json({
                message: 'person Info updated',
                data: person
            });
        });
    });
};
// Handle delete person
exports.delete = function (req, res) {
    person.remove({
        _id: req.params.person_id
    }, function (err, person) {
        if (err)
            res.send(err);
res.json({
            status: "success",
            message: 'person deleted'
        });
    });
};