// api-routes.js
// Initialize express router
let router = require('express').Router();
// Set default API response
router.get('/', function (req, res) {
    res.json({
        status: 'API Its Working',
        message: 'Welcome to RESTHub crafted with love!',
    });
});
// Import contact controller
var contactController = require('./contactController');
// Import utilisateur controller
var utilisateurController = require('./utilisateurController');
// Import person controller
var personController = require('./personController');
// Contact routes
router.route('/contacts')
    .get(contactController.index)
    .post(contactController.new);
router.route('/contacts/:contact_id')
    .get(contactController.view)
    .patch(contactController.update)
    .put(contactController.update)
    .delete(contactController.delete);
// Utilisateur routes
router.route('/utilisateurs')
    .get(utilisateurController.index)
    .post(utilisateurController.new);
router.route('/utilisateurs/login')
    .post(utilisateurController.login);
router.route('/utilisateurs/:utilisateur_id')
    .get(utilisateurController.view)
    .patch(utilisateurController.update)
    .put(utilisateurController.update)
    .delete(utilisateurController.delete);
// person routes
router.route('/persons')
    .get(personController.index)
    .post(personController.new);
router.route('/persons/bv/:bv_id')
    .get(personController.liste_person_bv);
router.route('/persons/:person_id')
    .get(personController.view)
    .patch(personController.update)
    .put(personController.update)
    .delete(personController.delete);


// Export API routes
module.exports = router;