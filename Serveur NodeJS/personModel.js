// personModel.js
let mongoose = require('mongoose');
// Setup schema
let personSchema = mongoose.Schema({
    nom : {
        type: String,
        required: true
    },
	prenom : String,
	dtn : {
        type: String,
        required: true
    },
	ln : {
        type: String,
        required: true
    },
	pere : {
        type: String,
        required: true
    },
	mere : {
        type: String,
        required: true
    },
    tel : [[String]
    ],
    cin : {
        numero : {
            type: String,
            minlength: 12,
            maxlength: 12
        },
        uid_nfc : {
            type: String,
            minlength: 20,
            maxlength: 20
        },
        photo : String,
        date_delivrance : [[String]
        ],
        lieu_delivrance : [[String]
        ],
        profession : [[String]
        ],
        domicile : [[String]
        ],
        fokontany : {
            id : String,
            val : String,
            desc : String
        },
        est_valide: Boolean
	},
	bv : {
		id : String,
		val : String,
		desc : String
	}
});
// Export Person model
let Person = module.exports = mongoose.model('person', personSchema);
module.exports.get = function (callback, limit) {
    Person.find(callback).limit(limit);
}