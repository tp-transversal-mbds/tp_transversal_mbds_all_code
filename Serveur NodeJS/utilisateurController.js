// utilisateurController.js
// Import utilisateur model
Utilisateur = require('./utilisateurModel');
// Handle index actions
exports.index = function (req, res) {
    Utilisateur.get(function (err, utilisateurs) {
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        }
        res.json({
            status: "success",
            message: "utilisateurs retrieved successfully",
            data: utilisateurs
        });
    });
};
// Handle create utilisateur actions
exports.new = function (req, res) {
    var utilisateur = new Utilisateur();
    utilisateur.nom = req.body.nom ? req.body.nom : utilisateur.nom;
    utilisateur.prenom = req.body.prenom;
    utilisateur.login = req.body.login;
    utilisateur.pass = req.body.pass;
    utilisateur.bv.id = req.body.bv.id
    utilisateur.bv.val = req.body.bv.val
    utilisateur.bv.desc = req.body.bv.desc
// save the utilisateur and check for errors
    utilisateur.save(function (err) {
        if (err)
           res.json(err);
res.json({
            message: 'New utilisateur created!',
            data: utilisateur
        });
    });
};
// Handle login utilisateur
exports.login = function (req, res) {
    Utilisateur.findOne({login: req.body.login, pass:req.body.pass}, function (err, utilisateur) {
        if (err)
            res.send(err);
        if(utilisateur!=null) {
            res.json({
                message: 'login succes',
                status: 200,
                data: utilisateur
            });
        } else {
            res.json({
                message: 'login ou mot de passe incorrect',
                status: 401,
                data: utilisateur
            });
        }
        
    });
};
// Handle view utilisateur info
exports.view = function (req, res) {
    Utilisateur.findById(req.params.utilisateur_id, function (err, utilisateur) {
        if (err)
            res.send(err);
        res.json({
            message: 'utilisateur details loading..',
            data: utilisateur
        });
    });
};

// Handle update utilisateur info
exports.update = function (req, res) {
utilisateur.findById(req.params.utilisateur_id, function (err, utilisateur) {
        if (err)
            res.send(err);
utilisateur.nom = req.body.nom ? req.body.nom : utilisateur.nom;
        utilisateur.prenom = req.body.prenom;
        utilisateur.login = req.body.login;
        utilisateur.pass = req.body.pass;
        utilisateur.bv.id = req.body.bv.id
        utilisateur.bv.val = req.body.bv.val
        utilisateur.bv.desc = req.body.bv.desc
// save the utilisateur and check for errors
        utilisateur.save(function (err) {
            if (err)
                res.json(err);
            res.json({
                message: 'utilisateur Info updated',
                data: utilisateur
            });
        });
    });
};
// Handle delete utilisateur
exports.delete = function (req, res) {
    utilisateur.remove({
        _id: req.params.utilisateur_id
    }, function (err, utilisateur) {
        if (err)
            res.send(err);
res.json({
            status: "success",
            message: 'utilisateur deleted'
        });
    });
};