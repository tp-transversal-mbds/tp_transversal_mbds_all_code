package com.creation.apps.mada.mefidy.service

import com.creation.apps.mada.mefidy.model.Electeur
import com.creation.apps.mada.mefidy.model.ElecteurJSON
import com.creation.apps.mada.mefidy.roomDao.ElecteurDatabase
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class ElecteurService {
    companion object {
        fun insertAllElecteur(compositeDisposable : CompositeDisposable, electeurDatabase: ElecteurDatabase, electeurs : List<Electeur>){
            //---------- * spread operator to convert List or Array to vararg
            compositeDisposable.add(Observable.fromCallable{electeurDatabase.electeurDao().insertAll(*electeurs.toTypedArray())}
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe())
        }

        fun updateElecteur(compositeDisposable : CompositeDisposable, electeurDatabase: ElecteurDatabase, electeur : Electeur){
            compositeDisposable.add(Observable.fromCallable{electeurDatabase.electeurDao().updateAll(electeur)}
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe())
        }

        fun parseElecteurJson(listElecteurJson : List<ElecteurJSON>) : List<Electeur>{
            var allElecteur : List<Electeur> = ArrayList<Electeur>()
            for(el in listElecteurJson){
                var electeur = Electeur(
                    _id = el._id,
                    nom = el.nom,
                    prenom = el.prenom,
                    cin = el.cin.numero,
                    uid = el.cin.uid_nfc,
                    photoUrl = el.cin.photo,
                    image = null,
                    aVoter = false
                )
                allElecteur += electeur
            }
            return allElecteur
        }
    }
}

