package com.creation.apps.mada.mefidy.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
class Electeur(
    @PrimaryKey var _id : String,
    @ColumnInfo var nom: String,
    @ColumnInfo var prenom: String,
    @ColumnInfo var cin : String,
    @ColumnInfo var uid : String,
    @ColumnInfo var photoUrl : String,
    @ColumnInfo(typeAffinity = ColumnInfo.BLOB)
    var image: ByteArray? = null,
    @ColumnInfo var aVoter : Boolean
)
