package com.creation.apps.mada.mefidy.activity

import android.app.PendingIntent
import android.content.Intent
import android.content.SharedPreferences
import android.nfc.NfcAdapter
import android.support.v7.app.AppCompatActivity

import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v4.widget.ContentLoadingProgressBar
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import com.creation.apps.mada.mefidy.R
import com.creation.apps.mada.mefidy.pager.SectionsPagerAdapter
import kotlinx.android.synthetic.main.activity_principal.*
import com.creation.apps.mada.mefidy.fragment.CheckCINFragment
import com.creation.apps.mada.mefidy.roomDao.ElecteurDatabase
import com.creation.apps.mada.mefidy.service.NFCService
import com.google.gson.Gson
import kotlinx.android.synthetic.main.list_cin.*


class PrincipalActivity : AppCompatActivity() {

    //-------------- Equivalent a http Session en web
    lateinit var session : SharedPreferences
    lateinit var gson : Gson

    lateinit var fragmentAdapter : SectionsPagerAdapter

    //--------- initialisation
    lateinit var nfcAdapter: NfcAdapter
    lateinit var pendingIntent: PendingIntent
    var isNfcSupported : Boolean = false

    //--------- tag uid et ndef
    lateinit var nfcTagNdef : String
    lateinit var nfcTagUID : String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_principal)
        setSupportActionBar(toolbar)

        session = PreferenceManager.getDefaultSharedPreferences(this)
        gson = Gson()

        //----------- tab menu
        fragmentAdapter = SectionsPagerAdapter(supportFragmentManager)
        container.adapter = fragmentAdapter
        tabs.setupWithViewPager(container)

        //----------- nfc initialisation
        nfcAdapter = NfcAdapter.getDefaultAdapter(this)
        pendingIntent = PendingIntent.getActivity(this, 0, Intent(this, javaClass).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0)
        isNfcSupported = nfcAdapter != null

    }

    override fun onPause() {
        super.onPause()
        nfcAdapter.disableForegroundDispatch(this)
        Log.d("debog","on pause principal")
    }

    override fun onResume() {
        super.onResume()
        nfcAdapter.enableForegroundDispatch(this, pendingIntent, null, null)
        Log.d("debog","on resume principal")
    }

    override fun onDestroy() {
        super.onDestroy()
        ElecteurDatabase.destroyInstance()
    }


    //------------ a l'approche du tag nfc
    override fun onNewIntent(intent: Intent) {
        Log.d("debog",intent.toString())
        Log.d("debog","on newIntent principal")
        //Méthode qui va traiter le contenu
        if (!isNfcSupported) {
            Toast.makeText(this, "Nfc is not supported on this device", Toast.LENGTH_SHORT).show()
        }else{
            if (!nfcAdapter.isEnabled) {
                Toast.makeText(this, "NFC disabled on this device. Turn on to proceed", Toast.LENGTH_SHORT).show()
            }else{
                //--------- get UID and CIN from nfcTag
                retrieveNFCMessage(intent)
                //--------- afficher l'électeur concerné
                val checkCinFragment =  fragmentAdapter.getItem(1) as CheckCINFragment
                checkCinFragment.scanCIN(this.nfcTagNdef,this.nfcTagUID)
                //--------- set active tab to checkCIN
                tabs.getTabAt(1)?.select()
            }
        }
    }

    //--------------------------------------------------//
    //---------------- lecture nfc ---------------------//
    //--------------------------------------------------//
    fun retrieveNFCMessage(intent: Intent) {

        if (NfcAdapter.ACTION_TAG_DISCOVERED == intent.action
            || NfcAdapter.ACTION_TECH_DISCOVERED == intent.action
            || NfcAdapter.ACTION_NDEF_DISCOVERED == intent.action) {
            Log.d("debog","ato am principal check tag")
            nfcTagNdef = NFCService.getNDefMessages(intent)
            nfcTagUID = NFCService.getUID(intent)
            Log.d("debog", "message: "+nfcTagNdef+" uid: "+nfcTagUID)
            Toast.makeText(this, "Scan du CIN Réussi", Toast.LENGTH_SHORT).show()

        } else {
            Toast.makeText(this, "Rapprocher le CIN pour le scanner", Toast.LENGTH_SHORT).show()
        }
    }



    //--------------------------------------------------//
    //---------------- logout action -------------------//
    //--------------------------------------------------//
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_principal, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == R.id.action_settings) {
            val intent = Intent(this, MainActivity::class.java)
            session.edit().putString("userConnecter","").commit()
            Toast.makeText(this,getString(R.string.logout), Toast.LENGTH_SHORT).show()
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent)
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}
