package com.creation.apps.mada.mefidy.pager

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.creation.apps.mada.mefidy.fragment.CheckCINFragment
import com.creation.apps.mada.mefidy.fragment.ListCINFragment

class SectionsPagerAdapter(fm : FragmentManager) : FragmentPagerAdapter(fm) {

    private val mFragments = ArrayList<Fragment>()

    init{
        mFragments.add(ListCINFragment())
        mFragments.add(CheckCINFragment())
    }

    override fun getItem(position: Int): Fragment {
        return mFragments.get(position);
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence {
        return when (position) {
            0 -> "Liste des CIN"
            else -> {
                "Scan CIN"
            }
        }
    }
}