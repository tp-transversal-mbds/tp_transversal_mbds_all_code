package com.creation.apps.mada.mefidy.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName

data class Utilisateur(
    var _id : String,
    var nom: String,
    var prenom: String,
    var login: String,
    var pass: String,
    var bv : BureauVote
)

data class JsonLogin(
    var data : Utilisateur
)

