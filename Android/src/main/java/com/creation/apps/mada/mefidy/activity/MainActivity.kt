package com.creation.apps.mada.mefidy.activity

import android.content.*
import android.net.ConnectivityManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import android.view.View
import android.widget.Toast
import com.creation.apps.mada.mefidy.R
import com.creation.apps.mada.mefidy.model.Utilisateur
import com.creation.apps.mada.mefidy.retrofitAPI.IElectionAPI
import com.creation.apps.mada.mefidy.retrofitAPI.RetrofitClient
import com.creation.apps.mada.mefidy.service.UtilService
import com.google.gson.Gson
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_login.*

class MainActivity : AppCompatActivity() {

    //-------------- Equivalent a http Session en web
    lateinit var session : SharedPreferences
    lateinit var gson : Gson
    lateinit var user : Utilisateur

    //-------------- consume Election API
    internal lateinit  var jsonApi: IElectionAPI
    //-------------- to make easy aync task
    internal var compositeDisposableElecteur = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        session = PreferenceManager.getDefaultSharedPreferences(this)
        gson = Gson()

        //----------- get Input value onclick Button Login
        //-------------- get all electeur by API
        val retrofit = RetrofitClient.instance
        jsonApi = retrofit.create(IElectionAPI::class.java)

        //----------- si la session contient un utilisateur on redirige
        // sinon on s'authentifie
        if(session.getString("userConnecter","") != ""){
            redirect()
        }else{
            login()
        }

    }

    fun login(){
        btnLogin.setOnClickListener{
            Log.d("debog","click login button")

            //-------- get input text
            var username : String = inputUsername.text.toString()
            var password : String = inputPassword.text.toString()

            Log.d("debog","username: "+username+" password :"+password)

            if(username != "" && password != ""){
                //-------- login service
                loginAPI(username, password)

            }else{
                Toast.makeText(this,"nom d'utilisateur ou mot de passe incorrect", Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun loginAPI(username : String, password : String){
        //-------- call API
        compositeDisposableElecteur.add(jsonApi.login(username,password)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({jsonLogin->
                if(jsonLogin.data != null){
                    user = jsonLogin.data
                    Log.d("debog",user._id)
                    Log.d("debog",user.bv.name)

                    session.edit().putString("userConnecter",gson.toJson(user)).commit()

                    //-------- after all redirect to Principal activity
                    Toast.makeText(this,"Connexion Réussi", Toast.LENGTH_SHORT).show()
                    redirect()
                }else{
                    Toast.makeText(this,"nom d'utilisateur ou mot de passe incorrect", Toast.LENGTH_SHORT).show()
                }
            },{
                Toast.makeText(this,"Connexion au serveur impossible", Toast.LENGTH_SHORT).show()
            })
        )
    }

    fun redirect(){
        val intent = Intent(this, PrincipalActivity::class.java)
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent)
    }

    //--------------------- check internet connexion
    override fun onStart() {
        super.onStart()
        registerReceiver(broadcastReceiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
    }

    override fun onStop() {
        super.onStop()
        unregisterReceiver(broadcastReceiver)
    }

    private var broadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val notConnected = intent.getBooleanExtra(
                ConnectivityManager.EXTRA_NO_CONNECTIVITY, false)
            if (notConnected) {
               internetConnexionError.visibility = View.VISIBLE
            } else {
                internetConnexionError.visibility = View.INVISIBLE
            }
        }
    }




}
