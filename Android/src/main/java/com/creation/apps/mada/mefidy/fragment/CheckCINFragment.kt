package com.creation.apps.mada.mefidy.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.creation.apps.mada.mefidy.R
import android.util.Log
import com.creation.apps.mada.mefidy.model.Electeur
import com.creation.apps.mada.mefidy.roomDao.ElecteurDatabase
import com.creation.apps.mada.mefidy.service.ElecteurService
import com.creation.apps.mada.mefidy.service.UtilService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.list_item_cin.view.*


class CheckCINFragment  : Fragment(){

    lateinit var defaultMessageCIN : TextView
    lateinit var aVoterMessage : TextView
    lateinit var nom : TextView
    lateinit var prenom : TextView
    lateinit var cin : TextView
    lateinit var imageProfilView : ImageView

    //-------------- to make easy aync task
    internal var compositeDisposableElecteur = CompositeDisposable()

    //--------- database sqllite
    lateinit var electeurDatabase: ElecteurDatabase

    var electeurScanner : Electeur? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val currentView : View = inflater.inflate(R.layout.check_cin, container, false)
        //----------- au demarrage
        initialize(currentView)

        //----------- database initialisation
        electeurDatabase = ElecteurDatabase.getInstance(activity!!)!!

        return currentView
    }

    fun initialize(currentView : View){
        defaultMessageCIN = currentView.findViewById(R.id.defaultMessageCIN) as TextView
        aVoterMessage = currentView.findViewById(R.id.aVoterMessage) as TextView
        nom = currentView.findViewById(R.id.nom) as TextView
        prenom = currentView.findViewById(R.id.prenom) as TextView
        cin = currentView.findViewById(R.id.cin) as TextView
        imageProfilView = currentView.findViewById(com.creation.apps.mada.mefidy.R.id.imageProfilView) as ImageView

        nom.visibility = View.INVISIBLE
        prenom.visibility = View.INVISIBLE
        cin.visibility = View.INVISIBLE
        imageProfilView.visibility = View.INVISIBLE
        aVoterMessage.visibility = View.INVISIBLE
    }

    fun scanCIN(nfcTagNdef : String, nfcTagUID : String) {
        getElecteurByCinAndUid(nfcTagNdef,nfcTagUID)
    }

    fun activeError(){
        nom.visibility = View.VISIBLE
        prenom.visibility = View.VISIBLE
        cin.visibility = View.VISIBLE
        imageProfilView.visibility = View.VISIBLE
        defaultMessageCIN.visibility = View.INVISIBLE
        aVoterMessage.visibility = View.VISIBLE
    }

    fun activeErrorNotInDB(){
        nom.visibility = View.INVISIBLE
        prenom.visibility = View.INVISIBLE
        cin.visibility = View.INVISIBLE
        imageProfilView.visibility = View.INVISIBLE
        defaultMessageCIN.visibility = View.VISIBLE
        aVoterMessage.visibility = View.INVISIBLE
    }

    fun activeSuccess(){
        nom.visibility = View.VISIBLE
        prenom.visibility = View.VISIBLE
        cin.visibility = View.VISIBLE
        imageProfilView.visibility = View.VISIBLE
        defaultMessageCIN.visibility = View.INVISIBLE
        aVoterMessage.visibility = View.INVISIBLE
    }

    private fun getElecteurByCinAndUid(nfcTagNdef : String, nfcTagUID : String){
        compositeDisposableElecteur.add(electeurDatabase.electeurDao().findByCinAndUid(nfcTagNdef,nfcTagUID)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    if(it.aVoter){
                        electeurScanner = it
                        //-------- si le resultat est vide
                        Log.d("debog","------------ deja voter")
                        activeError()
                        nom.text = electeurScanner!!.nom
                        prenom.text = electeurScanner!!.prenom
                        cin.text = electeurScanner!!.cin
                        val bitmap= UtilService.getBitmapFromByte(electeurScanner!!.image!!)
                        imageProfilView.setImageBitmap(bitmap)
                    }else{
                        electeurScanner = it
                        Log.d("debog","----------- enregistrer dans le bv")
                        activeSuccess()
                        nom.text = electeurScanner!!.nom
                        prenom.text = electeurScanner!!.prenom
                        cin.text = electeurScanner!!.cin
                        val bitmap= UtilService.getBitmapFromByte(electeurScanner!!.image!!)
                        imageProfilView.setImageBitmap(bitmap)
                        //----------- update aVoter
                        it.aVoter = true
                        ElecteurService.updateElecteur(compositeDisposableElecteur,electeurDatabase,it)
                    }

                },{},{
                    //-------- si le resultat est vide
                    Log.d("debog","------------ non enregistrer bv")
                    activeErrorNotInDB()
                    defaultMessageCIN.text = getString(R.string.error_message_non_bv)
                }
            )
        )
    }



}